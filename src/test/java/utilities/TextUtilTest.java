package utilities;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Danopie on 11/19/2017.
 */
public class TextUtilTest {
    @Test
    public void testGetFormattedText_InputInteger(){
        assertEquals("23", TextUtil.getFormattedText(23));
    }

    @Test
    public void testGetFormattedText_InputZero(){
        assertEquals("0", TextUtil.getFormattedText(0));
    }

    @Test
    public void testGetFormattedText_InputFloat(){
        assertEquals("10.5", TextUtil.getFormattedText(10.5));
    }

    @Test
    public void testGetFormattedText_InputFloatZero(){
        assertEquals("21", TextUtil.getFormattedText(21.0));
    }

    @Test
    public void testIsInString_CharInString(){
        assertTrue(TextUtil.isInString("Hello",'l'));
    }

    @Test
    public void testIsInString_CharNotInString(){
        assertFalse(TextUtil.isInString("Hello",'U'));
    }
}