package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Danopie on 11/21/2017.
 */
public class CalculatorTest {
    Calculator cal = new Calculator("Calculator");
    @Test
    public void testCalculator_InitProperly(){
        assertNotNull(cal);
    }

    @Test
    public void testCalculator_ButtonsInitProperly(){
        assertNotNull(cal.digitButton);
        assertNotNull(cal.memoryButton);
        assertNotNull(cal.specialButton);
        assertNotNull(cal.memoryButton);
    }
    @Test
    public void testCalculator_LabelsInitProperly(){
        assertNotNull(cal.displayLabel);
        assertNotNull(cal.memLabel);
    }

}