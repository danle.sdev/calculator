package calculator;

import calculator.button.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/*********************************************/

public class Calculator extends Frame {
    private static final int FRAME_WIDTH = 300, FRAME_HEIGHT = 325;
    private static final int HEIGHT = 30, WIDTH = 30, H_SPACE = 10, V_SPACE = 10;
    private static final int TOPX = 30, TOPY = 50;

    public boolean setClear = true;
    public double number, memValue;
    public char op;

    String digitButtonText[] = {"7", "8", "9", "4", "5", "6", "1", "2", "3", "0", "+/-", "."};
    String operatorButtonText[] = {"/", "sqrt", "*", "%", "-", "1/X", "+", "="};
    String memoryButtonText[] = {"MC", "MR", "MS", "M+"};
    String specialButtonText[] = {"Backspc", "C", "CE"};

    DigitButton digitButton[] = new DigitButton[digitButtonText.length];
    OperatorButton operatorButton[] = new OperatorButton[operatorButtonText.length];
    MemoryButton memoryButton[] = new MemoryButton[memoryButtonText.length];
    SpecialButton specialButton[] = new SpecialButton[specialButtonText.length];

    Label displayLabel = new Label("0", Label.RIGHT);
    Label memLabel = new Label(" ", Label.RIGHT);

    public String getDisplayLabel() {
        return displayLabel.getText();
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel.setText(displayLabel);
    }

    public String getMemLabel() {
        return memLabel.getText();
    }

    public void setMemLabel(String memLabel) {
        this.memLabel.setText(memLabel);
    }

    public Calculator(String frameText)//constructor
    {
        super(frameText);
        initializeUIComponents();
        setAdditionalProperties();
        addListeners();
    }

    private void initializeUIComponents() {
        initializeLabels();
        initializeMemoryButtons();
        initializeSpecialButtons();
        initializeDigitButtons();
        initializeOperatorButtons();
    }

    private void setAdditionalProperties() {
        setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setVisible(true);
        setResizable(false);
    }

    private void addListeners() {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                System.exit(0);
            }
        });
    }

    private void initializeOperatorButtons() {
        int opsX = TOPX + 3 * WIDTH + 4 * H_SPACE;
        int opsY = TOPY + 2 * (HEIGHT + V_SPACE);
        int tempX = opsX;
        int tempY = opsY;
        for (int i = 0; i < operatorButton.length; i++) {
            tempX += WIDTH + H_SPACE;
            operatorButton[i] = new OperatorButton(tempX, tempY, WIDTH, HEIGHT, operatorButtonText[i], this);
            operatorButton[i].setForeground(Color.RED);
            if ((i + 1) % 2 == 0) {
                tempX = opsX;
                tempY += HEIGHT + V_SPACE;
            }
        }
    }

    private void initializeDigitButtons() {
        int digitX = TOPX + WIDTH + H_SPACE;
        int digitY = TOPY + 2 * (HEIGHT + V_SPACE);
        int tempX = digitX;
        int tempY = digitY;
        for (int i = 0; i < digitButton.length; i++) {
            digitButton[i] = new DigitButton(tempX, tempY, WIDTH, HEIGHT, digitButtonText[i], this);
            digitButton[i].setForeground(Color.BLUE);
            tempX += WIDTH + H_SPACE;
            if ((i + 1) % 3 == 0) {
                tempX = digitX;
                tempY += HEIGHT + V_SPACE;
            }
        }
    }

    private void initializeSpecialButtons() {
        int tempX = TOPX + 1 * (WIDTH + H_SPACE);
        int tempY = TOPY + 1 * (HEIGHT + V_SPACE);
        for (int i = 0; i < specialButton.length; i++) {
            specialButton[i] = new SpecialButton(tempX, tempY, WIDTH * 2, HEIGHT, specialButtonText[i], this);
            specialButton[i].setForeground(Color.RED);
            tempX = tempX + 2 * WIDTH + H_SPACE;
        }
    }

    private void initializeMemoryButtons() {
        int tempX = TOPX;
        int tempY = TOPY + 2 * (HEIGHT + V_SPACE);
        for (int i = 0; i < memoryButton.length; i++) {
            memoryButton[i] = new MemoryButton(tempX, tempY, WIDTH, HEIGHT, memoryButtonText[i], this);
            memoryButton[i].setForeground(Color.RED);
            tempY += HEIGHT + V_SPACE;
        }
    }

    private void initializeLabels() {
        int tempX = TOPX;
        int tempY = TOPY;
        displayLabel.setBounds(tempX, tempY, 240, HEIGHT);
        displayLabel.setBackground(Color.BLACK);
        displayLabel.setForeground(Color.WHITE);
        add(displayLabel);

        memLabel.setBounds(TOPX, TOPY + HEIGHT + V_SPACE, WIDTH, HEIGHT);
        add(memLabel);
    }

}
