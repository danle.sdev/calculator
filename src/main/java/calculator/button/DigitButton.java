package calculator.button;

import calculator.Calculator;
import utilities.TextUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*******************************************/

public class DigitButton extends Button implements ActionListener {
    Calculator cl;

    public DigitButton(int x, int y, int width, int height, String cap, Calculator clc) {
        super(cap);
        setBounds(x, y, width, height);
        addActionListener(this);
        this.cl = clc;
        this.cl.add(this);
    }

    public void actionPerformed(ActionEvent ev) {
        String tempText = ((DigitButton) ev.getSource()).getLabel();

        if (tempText.equals(".")) {
            if (cl.setClear) {
                cl.setDisplayLabel("0.");
                cl.setClear = false;
            } else if (!TextUtil.isInString(cl.getDisplayLabel(), '.'))
                cl.setDisplayLabel(cl.getDisplayLabel() + ".");
            return;
        }

        int index = 0;
        try {
            index = Integer.parseInt(tempText);
        } catch (NumberFormatException e) {
            return;
        }

        if (index == 0 && cl.getDisplayLabel().equals("0")) return;

        if (cl.setClear) {
            cl.setDisplayLabel("" + index);
            cl.setClear = false;
        } else
            cl.setDisplayLabel(cl.getDisplayLabel() + index);
    }
}
