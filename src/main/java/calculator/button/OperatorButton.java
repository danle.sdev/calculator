package calculator.button;

import calculator.Calculator;
import utilities.TextUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/********************************************/

public class OperatorButton extends Button implements ActionListener {
    Calculator cl;

    public OperatorButton(int x, int y, int width, int height, String cap, Calculator clc) {
        super(cap);
        setBounds(x, y, width, height);
        this.cl = clc;
        this.cl.add(this);
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent ev) {
        String opText = ((OperatorButton) ev.getSource()).getLabel();

        cl.setClear = true;
        double temp = Double.parseDouble(cl.getDisplayLabel());

        if (opText.equals("1/x")) {
            try {
                double tempd = 1 / (double) temp;
                cl.setDisplayLabel(TextUtil.getFormattedText(tempd));
            } catch (ArithmeticException excp) {
                cl.setDisplayLabel("Divide by 0.");
            }
            return;
        }
        if (opText.equals("sqrt")) {
            try {
                double tempd = Math.sqrt(temp);
                cl.setDisplayLabel(TextUtil.getFormattedText(tempd));
            } catch (ArithmeticException excp) {
                cl.setDisplayLabel("Divide by 0.");
            }
            return;
        }
        if (!opText.equals("=")) {
            cl.number = temp;
            cl.op = opText.charAt(0);
            return;
        }
        switch (cl.op) {
            case '+':
                temp += cl.number;
                break;
            case '-':
                temp = cl.number - temp;
                break;
            case '*':
                temp *= cl.number;
                break;
            case '%':
                try {
                    temp = cl.number % temp;
                } catch (ArithmeticException excp) {
                    cl.setDisplayLabel("Divide by 0.");
                    return;
                }
                break;
            case '/':
                try {
                    temp = cl.number / temp;
                } catch (ArithmeticException excp) {
                    cl.setDisplayLabel("Divide by 0.");
                    return;
                }
                break;
        }
        cl.setDisplayLabel(TextUtil.getFormattedText(temp));
    }
}
