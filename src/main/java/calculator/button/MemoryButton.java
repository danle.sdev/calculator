package calculator.button;

import calculator.Calculator;
import utilities.TextUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/****************************************/

public class MemoryButton extends Button implements ActionListener {
    Calculator cl;

    /////////////////////////////////
    public MemoryButton(int x, int y, int width, int height, String cap, Calculator clc) {
        super(cap);
        setBounds(x, y, width, height);
        this.cl = clc;
        this.cl.add(this);
        addActionListener(this);
    }

    ////////////////////////////////////////////////
    public void actionPerformed(ActionEvent ev) {
        char memop = ((MemoryButton) ev.getSource()).getLabel().charAt(1);

        cl.setClear = true;
        double temp = Double.parseDouble(cl.getDisplayLabel());

        switch (memop) {
            case 'C':
                cl.setMemLabel(" ");
                cl.memValue = 0.0;
                break;
            case 'R':
                cl.setDisplayLabel(TextUtil.getFormattedText(cl.memValue));
                break;
            case 'S':
                cl.memValue = 0.0;
            case '+':
                cl.memValue += Double.parseDouble(cl.getDisplayLabel());
                if (cl.getDisplayLabel().equals("0") || cl.getDisplayLabel().equals("0.0"))
                    cl.setMemLabel(" ");
                else
                    cl.setMemLabel("M");
                break;
        }//switch
    }//actionPerformed
}//class
